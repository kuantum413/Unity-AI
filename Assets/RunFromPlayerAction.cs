﻿using UnityEngine;
using System.Collections;

public class RunFromPlayerAction : GoapAction
{

    public RunFromPlayerAction()
    {
        addEffect("survive", true);
    }

    void Start()
    {
        aiInput = gameObject.GetComponentInParent<AIInput>();
        startTime = Time.time;
    }

    public override void reset()
    {
        timeout = Random.Range(1.0f, 3.0f);
        startTime = Time.time;
        targetPlayer = null;
    }

    public override bool isDone()
    {
        FirstPersonController player = target.GetComponentInChildren<FirstPersonController>();

        if (!player.isAttacking())
        {
            aiInput.StopVerticalWalking();
            aiInput.FacePlayer();
            return true;
        }

        return false;
    }

    public override bool requiresInRange()
    {
        return false; // yes we need to be near a tree
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        PlayerComponent[] players = (PlayerComponent[])UnityEngine.GameObject.FindObjectsOfType(typeof(PlayerComponent));
        PlayerComponent closest = null;
        float closestDist = 0;

        foreach (PlayerComponent player in players)
        {
            if (closest == null)
            {
                closest = player;
                closestDist = (player.gameObject.transform.position - agent.transform.position).magnitude;
            }
            else {
                float dist = (player.gameObject.transform.position - agent.transform.position).magnitude;
                if (dist < closestDist)
                {
                    closest = player;
                    closestDist = dist;
                }
            }
        }

        if (closest == null)
            return false;

        targetPlayer = closest;
        target = targetPlayer.gameObject;
        startingPlayerHealth = target.GetComponentInParent<FirstPersonController>().health;

        return closest != null;
    }

    public override bool perform(GameObject agent)
    {
        if (Time.time - startTime > timeout)
        {
            cost += 1;
            aiInput.StopVerticalWalking();
            aiInput.FacePlayer();
            return false;
        }

        aiInput.FaceAwayFromPlayer();
        aiInput.WalkForward();

        return true;
    }
}