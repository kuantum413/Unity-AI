﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class HealthScript : MonoBehaviour {
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;

	// Use this for initialization
	void Start () {
        currentHealth = startingHealth;
	
	}
	
	// Update is called once per frame

    public void TakeDamage (int amount)
    {

        currentHealth -= amount;

        healthSlider.value = currentHealth;
    }

    public void SetHealth(int health)
    {
        currentHealth = health;

        healthSlider.value = currentHealth;
		if (healthSlider.value <= 0) {
			SceneManager.LoadScene ("Title");
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
        }
    }
}
