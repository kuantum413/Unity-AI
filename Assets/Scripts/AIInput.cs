﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class AIInput : MonoBehaviour {
    public GameObject m_Player;
    private float m_HorizontalAxis;
    private float m_VerticalAxis;
    private bool m_Jump;
    private bool m_QuickAttack;
    private bool m_LongAttack;

    private bool m_FacePlayer = true;
    private Vector3 m_Facing; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	    //bool jump = CrossPlatformInputManager.GetButtonDown("Jump");
     //   bool quickAttack = CrossPlatformInputManager.GetButton("Fire1");
     //   bool longAttack = CrossPlatformInputManager.GetButton("Fire2");
     //   float verticalAxis = CrossPlatformInputManager.GetAxis("Vertical");
     //   float horizontalAxis = CrossPlatformInputManager.GetAxis("Horizontal");

     Vector3 playerDir = (m_Player.transform.position - transform.position).normalized;
     if (m_FacePlayer)
        {
            m_Facing = (m_Player.transform.position - transform.position).normalized;
        }
     else
        {
            m_Facing = -(m_Player.transform.position - transform.position).normalized;
        }
     

     //   if (jump)
     //   {
     //       StartJumping();
     //   }
     //   else
     //   {
     //       StopJumping();
     //   }

     //   if (quickAttack)
     //   {
     //       StartQuickAttacking();
     //   }
     //   else
     //   {
     //       StopQuickAttacking();
     //   }

     //   if (longAttack)
     //   {
     //       StartLongAttacking();
     //   }
     //   else
     //   {
     //       StopLongAttacking();
     //   }

     //   if (verticalAxis > 0.0f)
     //   {
     //       WalkForward();
     //   }
     //   else if (verticalAxis < 0.0f)
     //   {
     //       WalkBackward();
     //   }
     //   else
     //   {
     //       StopVerticalWalking();
     //   }

     //   if (horizontalAxis > 0.0f)
     //   {
     //       WalkRight();
     //   }
     //   else if (horizontalAxis < 0.0f)
     //   {
     //       WalkLeft();
     //   }
     //   else
     //   {
     //       StopHorizontalWalking();
     //   }
    }

    /***
    Setters
    ***/

    public void WalkForward()
    {
        m_VerticalAxis = 1.0f;
    }

    public void WalkBackward()
    {
        m_VerticalAxis = -1.0f;
    }

    public void WalkLeft()
    {
        m_HorizontalAxis = -1.0f;
    }

    public void WalkRight()
    {
        m_HorizontalAxis = 1.0f;
    }

    public void StopVerticalWalking()
    {
        m_VerticalAxis = 0.0f;
    }

    public void StopHorizontalWalking()
    {
        m_HorizontalAxis = 0.0f;
    }

    public void StopWalking()
    {
        m_VerticalAxis = 0.0f;
        m_HorizontalAxis = 0.0f;
    }

    public void StartJumping()
    {
        m_Jump = true;
    }

    public void StopJumping()
    {
        m_Jump = false;
    }

    public void StartQuickAttacking()
    {
        m_QuickAttack = true;
    }

    public void StopQuickAttacking()
    {
        m_QuickAttack = false;
    }

    public void StartLongAttacking()
    {
        m_LongAttack = true;
    }

    public void StopLongAttacking()
    {
        m_LongAttack = false;
    }

    public void FacePlayer()
    {
        m_FacePlayer = true;
    }

    public void FaceAwayFromPlayer()
    {
        m_FacePlayer = false;
    }

    /***
    Getters
    ***/

    public bool IsJumping()
    {
        return m_Jump;
    }

    public bool IsQuickAttacking()
    {
        return m_QuickAttack;
    }

    public bool IsLongAttacking()
    {
        return m_LongAttack;
    }

    public float GetHorizontalAxis()
    {
        return m_HorizontalAxis;
    }

    public float GetVerticalAxis()
    {
        return m_VerticalAxis;
    }

    public bool IsFacingPlayer()
    {
        return m_FacePlayer;
    }

    public Vector3 GetFacing()
    {
        return m_Facing;
    }
}
