using System;
using UnityEngine;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;


[RequireComponent(typeof (CharacterController))]
[RequireComponent(typeof (AudioSource))]
public class AIController : MonoBehaviour
{
    public int health = 100;
    public int maxHealth = 100;

    [SerializeField] private bool m_IsWalking;
    [SerializeField] private float m_WalkSpeed;
    [SerializeField] private float m_RunSpeed;
    [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
    [SerializeField] private float m_JumpSpeed;
    [SerializeField] private float m_StickToGroundForce;
    [SerializeField] private float m_GravityMultiplier;
    [SerializeField] private MouseLook m_MouseLook;
    [SerializeField] private float m_StepInterval;
    [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
    [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
    [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.
    [SerializeField] private AudioClip m_HurtSound;           // the sound played when character takes damage
    [SerializeField] private AudioClip[] m_SlashSounds;    // an array of slash sounds that will be randomly selected from.
    [SerializeField] private float m_SlashSoundDelay = 0.0f;  // An amount of time to wait before playing slash sounds.

    private bool m_Jump;
    private bool m_QuickAttack;
    private bool m_LongAttack;
    private float m_YRotation;
    private Vector2 m_Input;
    private Vector3 m_MoveDir = Vector3.zero;
    private CharacterController m_CharacterController;
    private CollisionFlags m_CollisionFlags;
    private bool m_PreviouslyGrounded;
    private Vector3 m_OriginalCameraPosition;
    private float m_StepCycle;
    private float m_NextStep;
    public bool m_Jumping;
    private bool m_QuickAttacking;
    private bool m_LongAttacking;
    private AudioSource m_AudioSource;
    private AIInput m_AIInput;
    private Animation m_Animation;
    public float m_rotateSpeed = 3f;
    public float range = 1.5f;

    // Use this for initialization
    private void Start()
    { 

        m_CharacterController = GetComponent<CharacterController>();
        m_StepCycle = 0f;
        m_NextStep = m_StepCycle/2f;
        m_Jumping = false;
        m_AudioSource = GetComponent<AudioSource>();
        m_AIInput = GetComponent<AIInput>();

        m_Animation = GetComponentInChildren<Animation>();
        m_Animation["Jump"].wrapMode = WrapMode.Once;
        m_Animation["Attack1"].wrapMode = WrapMode.Once;
        m_Animation["Attack2"].wrapMode = WrapMode.Once;
    }


    // Update is called once per frame
    private void Update()
    {
        RotateView();
        // the jump state needs to read here to make sure it is not missed
        if (!m_Jump)
        {
            m_Jump = m_AIInput.IsJumping();
        }

        // Check for attack input from the AI
        m_QuickAttack = m_AIInput.IsQuickAttacking();
        m_LongAttack = m_AIInput.IsLongAttacking();

        // Check for attack completion
        if (m_QuickAttacking)
        {
            if (!m_Animation.IsPlaying("Attack1"))
            {
                m_QuickAttacking = false;
            }
        }

        if (m_LongAttacking)
        {
            if (!m_Animation.IsPlaying("Attack2"))
            {
                m_LongAttacking = false;
            }
        }

        if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
        {
            PlayLandingSound();
            m_MoveDir.y = 0f;
            m_Jumping = false;
        }
        if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
        {
            m_MoveDir.y = 0f;
        }

        m_PreviouslyGrounded = m_CharacterController.isGrounded;

        if (!m_Animation.isPlaying)
        {
            playIdleAnimation();
        }
    }


    private void PlayLandingSound()
    {
        m_AudioSource.clip = m_LandSound;
        m_AudioSource.Play();
        m_NextStep = m_StepCycle + .5f;
    }


    private void FixedUpdate()
    {
        float speed;
        GetInput(out speed);
        // always move along the camera forward as it is the direction that it being aimed at
        Vector3 desiredMove = transform.forward*m_Input.y + transform.right*m_Input.x;

        // get a normal for the surface that is being touched to move along it
        RaycastHit hitInfo;
        Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                            m_CharacterController.height/2f, ~0, QueryTriggerInteraction.Ignore);
        desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

        m_MoveDir.x = desiredMove.x*speed;
        m_MoveDir.z = desiredMove.z*speed;


        if (m_CharacterController.isGrounded)
        {
            m_MoveDir.y = -m_StickToGroundForce;

            if (m_Jump)
            {
                m_MoveDir.y = m_JumpSpeed;
                PlayJumpSound();
                m_Jump = false;
                m_Jumping = true;
                playJumpingAnimation();
            }
        }
        else
        {
            m_MoveDir += Physics.gravity*m_GravityMultiplier*Time.fixedDeltaTime;
        }
        m_CollisionFlags = m_CharacterController.Move(m_MoveDir*Time.fixedDeltaTime);

        ProgressStepCycle(speed);

        // Start an attack if input is set.
        if (!(m_QuickAttacking || m_LongAttacking))
        {
            if (m_QuickAttack)
            {
                m_QuickAttack = false;
                m_QuickAttacking = true;
                playQuickAttackAnimation();

                Invoke("Attack", 0.5f);
            }
            else if (m_LongAttack)
            {
                m_LongAttack = false;
                m_LongAttacking = true;
                playLongAttackAnimation();

                Invoke("Attack", 0.5f);
            }
        }
    }


    private void PlayJumpSound()
    {
        m_AudioSource.clip = m_JumpSound;
        m_AudioSource.Play();
    }


    private void ProgressStepCycle(float speed)
    {
        if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
        {
            m_StepCycle += (m_CharacterController.velocity.magnitude + (speed*(m_IsWalking ? 1f : m_RunstepLenghten)))*
                            Time.fixedDeltaTime;
            playRunningAnimation();
        }
        else
        {
            playIdleAnimation();
        }

        if (!(m_StepCycle > m_NextStep))
        {
            return;
        }

        m_NextStep = m_StepCycle + m_StepInterval;

        PlayFootStepAudio();
    }


    private void PlayFootStepAudio()
    {
        if (!m_CharacterController.isGrounded)
        {
            return;
        }
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, m_FootstepSounds.Length);
        m_AudioSource.clip = m_FootstepSounds[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        m_FootstepSounds[n] = m_FootstepSounds[0];
        m_FootstepSounds[0] = m_AudioSource.clip;
    }

    private void PlaySlashAudio()
    {
        // pick & play a random slash sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, m_SlashSounds.Length);
        m_AudioSource.clip = m_SlashSounds[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        m_SlashSounds[n] = m_SlashSounds[0];
        m_SlashSounds[0] = m_AudioSource.clip;
    }

    private void GetInput(out float speed)
    {
        // Read input
        float horizontal = m_AIInput.GetHorizontalAxis();
        float vertical = m_AIInput.GetVerticalAxis();

#if !MOBILE_INPUT
        // On standalone builds, walk/run speed is modified by a key press.
        // keep track of whether or not the character is walking or running
        m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
        // set the desired speed to be walking or running
        speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
        m_Input = new Vector2(horizontal, vertical);

        // normalize input if it exceeds 1 in combined length:
        if (m_Input.sqrMagnitude > 1)
        {
            m_Input.Normalize();
        }
    }


    private void RotateView()
    {
        float step = m_rotateSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, m_AIInput.GetFacing(), step, 0.0f);
        transform.rotation = Quaternion.LookRotation(newDir);
        //transform.rotation = Quaternion.LookRotation(m_AIInput.GetFacing());
    }


    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        //dont move the rigidbody if the character is on top of it
        if (m_CollisionFlags == CollisionFlags.Below)
        {
            return;
        }

        if (body == null || body.isKinematic)
        {
            return;
        }
        body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
    }

    private void Attack()
    {
        GameObject target = DetectTarget();
        if (null != target)
        {
            OnAttack(target, 10);
        }
    }

    private GameObject DetectTarget()
    {
        // Cast our detection ray and see if an interactable object is in range.
        Ray ray = new Ray(transform.position, m_AIInput.GetFacing());
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && hit.distance <= range)
        {
            return hit.transform.gameObject;
        }
        return null;
    }

    private void OnAttack(GameObject target, int damage)
    {
        FirstPersonController firstPerson = target.GetComponentInParent<FirstPersonController>();
        if (null != firstPerson)
        {
            firstPerson.TakeDamage(damage);
        }
    }

    public void TakeDamage(int damage)
    {
        health = Math.Max(health - damage, 0);
        gameObject.GetComponent<HealthScript>().SetHealth((int)(100.0f * health / maxHealth));
        m_AudioSource.clip = m_HurtSound;
        m_AudioSource.Play();
    }

    private void playRunningAnimation()
    {
        if (m_Animation.IsPlaying("run"))
        {
            return;
        }

        if (m_Animation.IsPlaying("idle"))
        {
            m_Animation.CrossFade("Run");
        }
        else
        {
            m_Animation.CrossFadeQueued("Run");
        }
    }

    private void playIdleAnimation()
    {
        if (m_Animation.IsPlaying("idle"))
        {
            return;
        }

        if (m_Animation.IsPlaying("Run"))
        {
            m_Animation.CrossFade("idle", 0.3f);
        }
        else
        {
            m_Animation.CrossFadeQueued("idle");
        }
    }

    private void playQuickAttackAnimation()
    {
        if (m_Animation.IsPlaying("Attack1"))
        {
            return;
        }

        m_Animation.CrossFade("Attack1");
        Invoke("PlaySlashAudio", m_SlashSoundDelay);
    }

    private void playLongAttackAnimation()
    {
        if (m_Animation.IsPlaying("Attack2"))
        {
            return;
        }

        m_Animation.CrossFade("Attack2");
        Invoke("PlaySlashAudio", m_SlashSoundDelay);
    }

    private void playJumpingAnimation()
    {
        if (m_Animation.IsPlaying("Jump"))
        {
            return;
        }

        if (m_Animation.IsPlaying("Attack1") || m_Animation.IsPlaying("Attack2")) {
            m_Animation.CrossFadeQueued("Jump");
        }
        else
        {
            m_Animation.CrossFade("Jump");
        }
    }
}
