﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TitleScript : MonoBehaviour {

	public Canvas exitMenu;
	public Button startText;
	public Button exitText;
	// Use this for initialization
	void Start () {
		exitMenu = exitMenu.GetComponent<Canvas> ();
		startText = startText.GetComponent<Button> ();
		exitText = exitText.GetComponent<Button> ();
		exitMenu.enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
	}

	public void ExitPress() {
		exitMenu.enabled = true;
		startText.enabled = false;
		exitText.enabled = false; 
	}

	public void NoPress() {
		exitMenu.enabled = false;
		startText.enabled = true;
		exitText.enabled = true; 
	}

	public void StartGame() {
		SceneManager.LoadScene("Keenan");
	}

	public void ExitGame() {
		Application.Quit ();
	}

	// Update is called once per frame
	void Update () {

	}
}