﻿using UnityEngine;
using System.Collections;

public class GetBehindPlayerAction : GoapAction {

    private bool moveRight;
    private float angle;
    public bool walkRight;

    public GetBehindPlayerAction()
    {
        addEffect("behindPlayer", true);
    }

    void Start()
    {
        chooseRandomDirection();
        aiInput = gameObject.GetComponentInParent<AIInput>();
        startTime = Time.time;
    }

    public override void reset()
    {
        chooseRandomDirection();
        angle = 0.0f;
        timeout = Random.Range(1.0f, 3.0f);
        startTime = Time.time;
        targetPlayer = null;
    }

    public override bool isDone()
    {
        if (Mathf.Abs(angle) > 140.0f)
        {
            aiInput.StopHorizontalWalking();
            return true;
        }

        return false;
    }

    public override bool requiresInRange()
    {
        return false; // yes we need to be near a tree
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        PlayerComponent[] players = (PlayerComponent[])UnityEngine.GameObject.FindObjectsOfType(typeof(PlayerComponent));
        PlayerComponent closest = null;
        float closestDist = 0;

        foreach (PlayerComponent player in players)
        {
            if (closest == null)
            {
                closest = player;
                closestDist = (player.gameObject.transform.position - agent.transform.position).magnitude;
            }
            else {
                float dist = (player.gameObject.transform.position - agent.transform.position).magnitude;
                if (dist < closestDist)
                {
                    closest = player;
                    closestDist = dist;
                }
            }
        }

        if (closest == null)
            return false;

        targetPlayer = closest;
        target = targetPlayer.gameObject;
        startingPlayerHealth = target.GetComponentInParent<FirstPersonController>().health;

        return closest != null;
    }

    public override bool perform(GameObject agent)
    {
        Vector3 playerToAIDir = gameObject.transform.position - target.transform.position;
        Vector3 playerCameraDir = target.GetComponentInChildren<Camera>().transform.forward;
        angle = Vector3.Angle(playerCameraDir, playerToAIDir);

        if(walkRight)
        {
            aiInput.WalkRight();
        }
        else
        {
            aiInput.WalkLeft();
        }

        if (Time.time - startTime > timeout)
        {
            cost += 1;
            aiInput.StopHorizontalWalking();
            return false;
        }

        return true;
    }

    private void chooseRandomDirection()
    {
        walkRight = Random.Range(0.0f, 100.0f) < 50.0f;
    }
}