﻿using UnityEngine;
using System.Collections;

public class JumpAction : GoapAction {

    public JumpAction()
    {
        addEffect("behaveOddly", true);
    }

    void Start()
    {
        startingAIHealth = GetComponent<AIController>().health;
        aiInput = gameObject.GetComponentInParent<AIInput>();
        startTime = Time.time;
    }

    public override void reset()
    {
        startingAIHealth = GetComponent<AIController>().health;
        timeout = Random.Range(1.0f, 3.0f);
        startTime = Time.time;
        targetPlayer = null;
    }

    public override bool isDone()
    {
        if (GetComponent<AIController>().m_Jumping)
        {
            aiInput.StopJumping();
            return true;
        }

        return false;
    }

    public override bool requiresInRange()
    {
        return false; // yes we need to be near a tree
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        return true;
    }

    public override bool perform(GameObject agent)
    {
        aiInput.StartJumping();

        if (Time.time - startTime > timeout)
        {
            cost += 1;
            aiInput.StopJumping();
            return false;
        }

        if (GetComponent<AIController>().health < startingAIHealth)
        {
            cost += 1;
            aiInput.StopJumping();
            return false;
        }

        return true;
    }
}
