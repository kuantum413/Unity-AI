﻿    
using System;
using UnityEngine;

public class KillPlayerAction : GoapAction
{
    private bool playerDead;

	public KillPlayerAction ()
    {
        addPrecondition("behaveOddly", true);
		addPrecondition("hurtPlayer", true);
		addEffect ("playerDead", true);
	}

	void Start()
	{
		aiInput = gameObject.GetComponentInParent<AIInput>();
	}

	public override void reset ()
	{
		targetPlayer = null;
	}

	public override bool isDone ()
	{
		return playerDead;
	}

	public override bool requiresInRange ()
	{
		return false; // yes we need to be near a player
	}

	public override bool checkProceduralPrecondition (GameObject agent)
	{
		PlayerComponent[] players = (PlayerComponent[])UnityEngine.GameObject.FindObjectsOfType (typeof(PlayerComponent));
		PlayerComponent closest = null;
		float closestDist = 0;

		foreach (PlayerComponent player in players) {
			if (closest == null) {
				closest = player;
				closestDist = (player.gameObject.transform.position - agent.transform.position).magnitude;
			} else {
				float dist = (player.gameObject.transform.position - agent.transform.position).magnitude;
				if (dist < closestDist) {
					closest = player;
					closestDist = dist;
				}
			}
		}

		if (closest == null)
			return false;

		targetPlayer = closest;
		target = targetPlayer.gameObject;

		return closest != null;
	}

	public override bool perform (GameObject agent)
	{
		if (targetPlayer.GetComponentInParent<FirstPersonController> ().health <= 0) {
            playerDead = true;
			return true;
		}

		return false;
	}

}