﻿
using System;
using UnityEngine;

public class AttackPlayerFromBehindAction : AttackPlayerAction
{
    public AttackPlayerFromBehindAction()
    {
        addPrecondition("behindPlayer", true);
        addEffect("hurtPlayer", true);
    }
}