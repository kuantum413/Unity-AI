﻿
using System;
using UnityEngine;

public class AttackPlayerAction : GoapAction
{   
    private int maxAIHealth;
    public float atkRange = 1.7f;

	public AttackPlayerAction () {
		addEffect("hurtPlayer", true);
	}

    void Start()
    {
        AIController aiController = gameObject.GetComponent<AIController>();
        startingAIHealth = aiController.health;
        maxAIHealth = aiController.maxHealth;
        aiInput = gameObject.GetComponentInParent<AIInput>();
    }

    void Update()
    {
        if (startingAIHealth < maxAIHealth / 2.0f)
        {
            addPrecondition("survive", true);
        }
    }

	public override void reset ()
	{
        AIController aiController = gameObject.GetComponent<AIController>();
        startingAIHealth = aiController.health;
        maxAIHealth = aiController.maxHealth;
        targetPlayer = null;
	}

	public override bool isDone ()
	{
        float currentHealth = target.GetComponentInParent<FirstPersonController>().health;
        if (currentHealth < startingPlayerHealth || startingPlayerHealth == 0)
        {
            aiInput.StopQuickAttacking();
            return true;
        }
        return false;
	}

	public override bool requiresInRange ()
	{
		return true; // yes we need to be near a tree
	}

	public override bool checkProceduralPrecondition (GameObject agent)
	{
		// find the nearest tree that we can chop
		PlayerComponent[] players = (PlayerComponent[])UnityEngine.GameObject.FindObjectsOfType (typeof(PlayerComponent));
		PlayerComponent closest = null;
		float closestDist = 0;

		foreach (PlayerComponent player in players) {
			if (closest == null) {
				closest = player;
				closestDist = (player.gameObject.transform.position - agent.transform.position).magnitude;
			} else {
				float dist = (player.gameObject.transform.position - agent.transform.position).magnitude;
				if (dist < closestDist) {
					closest = player;
					closestDist = dist;
				}
			}
		}

		if (closest == null)
			return false;

		targetPlayer = closest;
        target = targetPlayer.gameObject;
        startingPlayerHealth = target.GetComponentInParent<FirstPersonController>().health;

		return closest != null;
	}

	public override bool perform (GameObject agent)
	{
		float distance = 0;

		if (UnityEngine.Random.Range (0, 100) < 70) {
			aiInput.StartQuickAttacking ();
		} else {
			aiInput.StartLongAttacking ();
		}

		distance = (gameObject.transform.position - target.gameObject.transform.position).magnitude;
		if (distance > atkRange || GetComponent<AIController> ().health < startingAIHealth) {
			cost += 1;
			aiInput.StopQuickAttacking ();
			aiInput.StopLongAttacking ();
			return false;
		}
		//} else {
		//	aiInput.StartLongAttacking ();

		//	distance = (gameObject.transform.position - target.gameObject.transform.position).magnitude;
		//	if (distance > atkRange || GetComponent<AIController> ().health < startingAIHealth) {
		//		cost += 1;
		//		aiInput.StopLongAttacking ();
		//		return false;
		//	}
		//}

		return true;
	}

}