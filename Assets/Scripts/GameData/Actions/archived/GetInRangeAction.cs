﻿using UnityEngine;
using System.Collections;

public class GetInRangeAction : GoapAction
{
    private float playerDist;
    public float range = 3.0f;

    public GetInRangeAction()
    {
        addEffect("inRange", true);
    }

    void Start()
    {
        startingAIHealth = gameObject.GetComponent<AIController>().health;
        aiInput = gameObject.GetComponentInParent<AIInput>();
        startTime = Time.time;
    }

    public override void reset()
    {
        startingAIHealth = GetComponent<AIController>().health;
        timeout = Random.Range(3.0f, 10.0f);
        startTime = Time.time;
        targetPlayer = null;
    }

    public override bool isDone()
    {
        if (playerDist < range)
        {
            aiInput.StopWalking();
            return true;
        }

        return false;
    }

    public override bool requiresInRange()
    {
        return false;
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        PlayerComponent[] players = (PlayerComponent[])UnityEngine.GameObject.FindObjectsOfType(typeof(PlayerComponent));
        PlayerComponent closest = null;
        float closestDist = 0;

        foreach (PlayerComponent player in players)
        {
            if (closest == null)
            {
                closest = player;
                closestDist = (player.gameObject.transform.position - agent.transform.position).magnitude;
            }
            else {
                float dist = (player.gameObject.transform.position - agent.transform.position).magnitude;
                if (dist < closestDist)
                {
                    closest = player;
                    closestDist = dist;
                }
            }
        }

        if (closest == null)
            return false;

        targetPlayer = closest;
        target = targetPlayer.gameObject;
        startingAIHealth = target.GetComponentInParent<FirstPersonController>().health;

        return closest != null;
    }

    public override bool perform(GameObject agent)
    {
        aiInput.WalkForward();

        playerDist = (transform.position - target.transform.position).magnitude;
        if (playerDist < range)
        {
            aiInput.StopWalking();
            return true;
        }

        if (GetComponent<AIController>().health < startingAIHealth)
        {
            cost += 1;
            aiInput.StopWalking();
            return false;
        }

        if (Time.time - startTime > timeout)
        {
            cost += 1;
            aiInput.StopWalking();
            return false;
        }

        return true;
    }
}
