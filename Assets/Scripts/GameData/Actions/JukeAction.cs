﻿using UnityEngine;
using System.Collections;

public class JukeAction : GoapAction
{
    private float dirTime;
    private float dirTimeout;
    private float playerDist;


    public JukeAction()
    {
        addEffect("behaveOddly", true);
    }

    void Start()
    {
        startingAIHealth = GetComponent<AIController>().health;
        aiInput = gameObject.GetComponentInParent<AIInput>();
        startTime = Time.time;
    }

    public override void reset()
    {
        startingAIHealth = GetComponent<AIController>().health;
        timeout = Random.Range(0.5f, 1.0f);
        startTime = Time.time;
        targetPlayer = null;
    }

    public override bool isDone()
    {
        if (Time.time - startTime > timeout)
        {
            aiInput.StopWalking();
            return true;
        }

        return false;
    }

    public override bool requiresInRange()
    {
        return false; // yes we need to be near a tree
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        PlayerComponent[] players = (PlayerComponent[])UnityEngine.GameObject.FindObjectsOfType(typeof(PlayerComponent));
        PlayerComponent closest = null;
        float closestDist = 0;

        foreach (PlayerComponent player in players)
        {
            if (closest == null)
            {
                closest = player;
                closestDist = (player.gameObject.transform.position - agent.transform.position).magnitude;
            }
            else {
                float dist = (player.gameObject.transform.position - agent.transform.position).magnitude;
                if (dist < closestDist)
                {
                    closest = player;
                    closestDist = dist;
                }
            }
        }

        if (closest == null)
            return false;

        targetPlayer = closest;
        target = targetPlayer.gameObject;
        startingPlayerHealth = target.GetComponentInParent<FirstPersonController>().health;

        return closest != null;
    }

    public override bool perform(GameObject agent)
    {
        playerDist = (transform.position - target.transform.position).magnitude;
        if (playerDist > 10.0f)
        {
            cost += 1;
            aiInput.StopWalking();
            return false;
        }

        if (GetComponent<AIController>().health < startingAIHealth)
        {
            cost += 1;
            aiInput.StopWalking();
            return false;
        }

        if (dirTime <= 0.0f || Time.time - dirTime > dirTimeout )
        {
            walkRandomDirection();
        }
    
        return true;
    }

    private void walkRandomDirection()
    {
        dirTime = Time.time;
        dirTimeout = Random.Range(0.3f, 1.0f);
        float walkDir = Random.Range(0.0f, 100.0f);

        if (walkDir <= 25.0f)
        {
            aiInput.WalkForward();
        }
        else if (walkDir <= 50.0f)
        {
            aiInput.WalkBackward();
        }
        else if (walkDir <= 75.0f)
        {
            aiInput.WalkLeft();
        }
        else
        {
            aiInput.WalkRight();
        }
    }
}