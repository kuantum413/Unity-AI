﻿using UnityEngine;
using System.Collections;

public class WaitForPlayerAction : GoapAction
{
    private float dirTime;
    private float dirTimeout;
    private float playerDist = -1;
    public float range = 3.0f;

    public WaitForPlayerAction()
    {
        addEffect("inRange", true);
    }

    void Start()
    {
        startingAIHealth = gameObject.GetComponent<AIController>().health;
        aiInput = gameObject.GetComponentInParent<AIInput>();
        startTime = Time.time;
    }

    public override void reset()
    {
        startingAIHealth = GetComponent<AIController>().health;
        timeout = Random.Range(3.0f, 10.0f);
        startTime = Time.time;
        targetPlayer = null;
    }

    public override bool isDone()
    {
        if (playerDist < range && playerDist > 0)
        {
            aiInput.StopWalking();
            return true;
        }

        return false;
    }

    public override bool requiresInRange()
    {
        return false;
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        PlayerComponent[] players = (PlayerComponent[])UnityEngine.GameObject.FindObjectsOfType(typeof(PlayerComponent));
        PlayerComponent closest = null;
        float closestDist = 0;

        foreach (PlayerComponent player in players)
        {
            if (closest == null)
            {
                closest = player;
                closestDist = (player.gameObject.transform.position - agent.transform.position).magnitude;
            }
            else {
                float dist = (player.gameObject.transform.position - agent.transform.position).magnitude;
                if (dist < closestDist)
                {
                    closest = player;
                    closestDist = dist;
                }
            }
        }

        if (closest == null)
            return false;

        targetPlayer = closest;
        target = targetPlayer.gameObject;
        startingPlayerHealth = target.GetComponentInParent<FirstPersonController>().health;

        return closest != null;
    }

    public override bool perform(GameObject agent)
    {
        playerDist = (transform.position - target.transform.position).magnitude;
        if (playerDist < range)
        {
            aiInput.StopWalking();
            return true;
        }

        if (GetComponent<AIController>().health < startingAIHealth)
        {
            cost += 1;
            aiInput.StopWalking();
            return false;
        }

        if (Time.time - startTime > timeout)
        {
            cost += 1;
            aiInput.StopWalking();
            return false;
        }

        if (dirTime <= 0.0f || Time.time - dirTime > dirTimeout)
        {
            walkRandomDirection();
        }

        return true;
    }

    private void walkRandomDirection()
    {
        dirTime = Time.time;
        dirTimeout = Random.Range(0.1f, 0.3f);
        float walkDir = Random.Range(0.0f, 100.0f);

        if (walkDir <= 25.0f)
        {
            aiInput.WalkForward();
        }
        else if (walkDir <= 50.0f)
        {
            aiInput.WalkBackward();
        }
        else if (walkDir <= 75.0f)
        {
            aiInput.WalkLeft();
        }
        else
        {
            aiInput.WalkRight();
        }
    }
}
