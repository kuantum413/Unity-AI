﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Samurai : Opponent {

    public override HashSet<KeyValuePair<string, object>> createGoalState()
    {
        HashSet<KeyValuePair<string, object>> goal = new HashSet<KeyValuePair<string, object>>();

        goal.Add(new KeyValuePair<string, object>("playerDead", true));

        if (aiController.health < aiController.maxHealth / 2)
        {
            goal.Add(new KeyValuePair<string, object>("survive", true));
        }

        return goal;
    }
}
