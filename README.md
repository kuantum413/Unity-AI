This is the Git repo for the Unity AI Opponent final project for CPE 480 Spring 2016.
A video can be found here: https://www.youtube.com/watch?v=s5WkJQSblrk

***
Steps to cloning the repo:
***

0. Create a GitLab account and give Keenan your username. You will be added as a Master user to the private repo.
1. Open terminal and check if you have an ssh key.
> cat ~/.ssh/id_rsa.pub

2. If you do, copy everything from ssh-rsa to the end of the line, and proceed to step 4.
3. If you don't, enter the below command and follow the instructions. Repeat step 2.
> ssh-keygen

4. On GitLab in the browser navigate to Home->Profile Settings->SSH Keys, and paste your copied key where prompted. Now you can access GitLab repos from your account on this computer.
5. In terminal, navigate to the directory you want to be the parent to the project directory and type below.
> git clone git@gitlab.com:kreimer314/Unity-AI.git

6. Now you have cloned the repo! In Unity open the folder Unity-AI as a project and you're good to go. Note: we are working on seperate scenes. Create a scene for yourself and drag this to the hierarchy, and feel free to remove the Unitled Scene.

***
Pulling and Pushing Changes
***

1. Whenever you start working type below. This updates you to the status of the remote repository.
> git pull

2. If there are merge conflicts in the Assets folder, then we have a problem. But, if there are merge conflicts in the ProjectSettings folder this is normal. Type below to get rid of them and pull again.
> git stash

3. When you are done working ALWAYS type below.
> git push origin master